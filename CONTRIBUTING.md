# How to Contribute

We'd love to accept your patches and contributions to this project. This
document outlines the requirements for contributing, and hopefully provides
some information on how to make a successful contribution.

## First-time contributors: Attribution and copyright
### Authors file

We maintain an `AUTHORS.md` file in the project root. If you are not listed
in the file, please add your name (and, if your contributions are owned by
a company, that company) as part of your first contribution.

## Code formatting

All Rust code in this project uses
[`rustfmt`](https://github.com/rust-lang/rustfmt) for code formatting; *each
commit* must be formatted with `rustfmt`, as we do not want the git history
polluted with formatting changes unrelated to the primary purpose of the
commit. To facilitate this, it is recommended that you install our pre-commit
hooks using [`pre-commit`](https://pre-commit.com/). To do so, install
`pre-commit` and run `pre-commit install` in the repository root before making
your first commit.

