# List of Authors

This is a list of the authors of the `metadata-backup` who have contributed code, documentation or any other copyrightable material. Those who have submitted code on behalf of a corporation will be listed with their corporation listed in parentheses, e.g. Kathy Z. Monstertruck (Widget Co).

1. Paul Ganssle (Google LLC)
