// Copyright 2019 metadata-backup Authors (see AUTHORS.md)

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::time::Instant;

use structopt::StructOpt;

use metadata_backup::backup;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short = "r", parse(from_os_str))]
    root: std::path::PathBuf,
    #[structopt(short = "o", parse(from_os_str))]
    output: std::path::PathBuf,
    #[structopt(short = "", long = "no-remove-on-failure")]
    no_remove_on_failure: bool,
}

fn main() {
    let args = Cli::from_args();

    let file_root = args.root;
    let out_path = args.output;

    println!(
        "Backing up {} to {}.",
        file_root.to_str().unwrap(),
        out_path.to_str().unwrap()
    );
    let t = Instant::now();
    match backup::write_backup(&file_root, &out_path) {
        Ok(_) => {}
        Err(e) => {
            println!("Error during backup: {}", e);
            if out_path.exists() && !args.no_remove_on_failure {
                std::fs::remove_file(&out_path).unwrap();
            }
        }
    };

    println!(
        "Backup complete in {} seconds.",
        (Instant::now() - t).as_secs()
    );
}
